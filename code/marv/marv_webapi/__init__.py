# Copyright 2016 - 2021  Ternaris.
# SPDX-License-Identifier: AGPL-3.0-only

from pkg_resources import iter_entry_points

from . import api, auth, collection, comment
from . import dataset as dataset_api
from . import delete, rpcs, tag
